﻿#include "pch.h"
#include "CppUnitTest.h"
#include "../LAB1_3sem/Sorter.h"
#include "../LAB1_3sem/Sequence.h"
#include <fstream>
#include "../LAB1_3sem/IntTimeMarker.h"
#define forn(i, j, k) for(int i = j; i < k; i++)

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SorterTests
{
	TEST_CLASS(BubbleSortTests)
	{
	public:
		TEST_METHOD(BubbleSort_empty)
		{
			LListSequence<int> a;
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::BubbleSort(a, pred);
		}
		TEST_METHOD(BubbleSort_threeSameValues_straight)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] <= a[i + 1]);
		}
		TEST_METHOD(BubbleSort_threeSameValues_back)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] >= a[i + 1]);
		}
		TEST_METHOD(BubbleSort_14straigt)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(BubbleSort_14back)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(BubbleSort_14OtherPredicate_back)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(BubbleSort_14OtherPredicate_straight)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(BubbleSort_LListRandom2000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 2000;
			LListSequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] >= a[i + 1]);

			f.close();
		}
		TEST_METHOD(BubbleSort_ArrayRandom10000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 10000;
			ArraySequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::BubbleSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] <= a[i + 1]);

			f.close();
		}
	};

	TEST_CLASS(ShakerSortTests)
	{
	public:
		TEST_METHOD(ShakerSort_empty)
		{
			LListSequence<int> a;
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::ShakerSort(a, pred);
		}
		TEST_METHOD(ShakerSort_threeSameValues_straight)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] <= a[i + 1]);
		}
		TEST_METHOD(ShakerSort_threeSameValues_back)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] >= a[i + 1]);
		}
		TEST_METHOD(ShakerSort_14straigt)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(ShakerSort_14back)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(ShakerSort_14OtherPredicate_back)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(ShakerSort_14OtherPredicate_straight)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(ShakerSort_LListRandom2000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 2000;
			LListSequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] >= a[i + 1]);

			f.close();
		}
		TEST_METHOD(ShakerSort_ArrayRandom10000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 10000;
			ArraySequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::ShakerSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] <= a[i + 1]);

			f.close();
		}
	};

	TEST_CLASS(SimpleInsertionsTests)
	{
	public:
		TEST_METHOD(SimpleInsertionsSort_empty)
		{
			LListSequence<int> a;
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::SimpleInsertionsSort(a, pred);
		}
		TEST_METHOD(SimpleInsertionsSort_threeSameValues_straight)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] <= a[i + 1]);
		}
		TEST_METHOD(SimpleInsertionsSort_threeSameValues_back)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] >= a[i + 1]);
		}
		TEST_METHOD(SimpleInsertionsSort_14straigt)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(SimpleInsertionsSort_14back)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(SimpleInsertionsSort_14OtherPredicate_back)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(SimpleInsertionsSort_14OtherPredicate_straight)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(SimpleInsertionsSort_LListRandom2000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 2000;
			LListSequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] >= a[i + 1]);

			f.close();
		}
		TEST_METHOD(SimpleInsertionsSort_ArrayRandom10000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 10000;
			ArraySequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::SimpleInsertionsSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] <= a[i + 1]);

			f.close();
		}
	};

	TEST_CLASS(SimpleSelectionSortTests)
	{
	public:
		TEST_METHOD(SimpleSelectionSort_empty)
		{
			LListSequence<int> a;
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::SimpleSelectionSort(a, pred);
		}
		TEST_METHOD(SimpleSelectionSort_threeSameValues_straight)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] <= a[i + 1]);
		}
		TEST_METHOD(SimpleSelectionSort_threeSameValues_back)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] >= a[i + 1]);
		}
		TEST_METHOD(SimpleSelectionSort_14straigt)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(SimpleSelectionSort_14back)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(SimpleSelectionSort_14OtherPredicate_back)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(SimpleSelectionSort_14OtherPredicate_straight)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(SimpleSelectionSort_LListRandom2000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 2000;
			LListSequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] >= a[i + 1]);

			f.close();
		}
		TEST_METHOD(SimpleSelectionSort_ArrayRandom10000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 10000;
			ArraySequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::SimpleSelectionSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] <= a[i + 1]);

			f.close();
		}
	};

	TEST_CLASS(BinaryInsertionsSortTests)
	{
	public:
		TEST_METHOD(BinaryInsertionsSort_empty)
		{
			LListSequence<int> a;
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::BinaryInsertionsSort(a, pred);
		}
		TEST_METHOD(BinaryInsertionsSort_threeSameValues_straight)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] <= a[i + 1]);
		}
		TEST_METHOD(BinaryInsertionsSort_threeSameValues_back)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] >= a[i + 1]);
		}
		TEST_METHOD(BinaryInsertionsSort_14straigt)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(BinaryInsertionsSort_14back)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(BinaryInsertionsSort_14OtherPredicate_back)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(BinaryInsertionsSort_14OtherPredicate_straight)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(BinaryInsertionsSort_LListRandom2000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 2000;
			LListSequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] >= a[i + 1]);

			f.close();
		}
		TEST_METHOD(BinaryInsertionsSort_ArrayRandom10000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 10000;
			ArraySequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::BinaryInsertionsSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] <= a[i + 1]);

			f.close();
		}
	};

	TEST_CLASS(QuickSortTests)
	{
	public:
		TEST_METHOD(QuickSort_empty)
		{
			LListSequence<int> a;
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::QuickSort(a, pred);
		}
		TEST_METHOD(QuickSort_threeSameValues_straight)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] <= a[i + 1]);
		}
		TEST_METHOD(QuickSort_threeSameValues_back)
		{
			ArraySequence<int> a;
			a.Append(-1);
			forn(i, 0, 4)
				a.Append(0);
			a.Append(1);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, 5)
				Assert::IsTrue(a[i] >= a[i + 1]);
		}
		TEST_METHOD(QuickSort_14straigt)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<LListSequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(QuickSort_14back)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] < a[i + 1]);
		}
		TEST_METHOD(QuickSort_14OtherPredicate_back)
		{
			LListSequence<int> a;
			forn(i, 0, 14)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(QuickSort_14OtherPredicate_straight)
		{
			ArraySequence<int> a;
			for (int i = 14; i > 0; i--)
				a.Append(i);
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<ArraySequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, 13)
				Assert::IsTrue(a[i] > a[i + 1]);
		}
		TEST_METHOD(QuickSort_LListRandom2000)		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 2000;
			LListSequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a < b; };
			Sorter<LListSequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] >= a[i + 1]);

			f.close();
		}
		TEST_METHOD(QuickSort_ArrayRandom10000)
		{
			ifstream f;
			f.open("../Tests/Text.txt");
			const int Number = 10000;
			ArraySequence<int> a;
			for (int i = 0; i < Number; i++)
			{
				int c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(int, int) = [](int a, int b) {return a > b; };
			Sorter<ArraySequence<int>, int>::QuickSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] <= a[i + 1]);

			f.close();
		}
	};

	TEST_CLASS(AlternativeTypesTest)
	{
		//By Real Then By Imaginary -- bRTbI
		TEST_METHOD(Complex_bRTbI)
		{
			ArraySequence<Complex> seq;
			for (int i = 0; i < 10; i++)
				seq.Append(Complex(i / 5, i % 5));
			bool (*pred)(Complex, Complex) = [](Complex a, Complex b) {return a.getReal() < b.getReal() || (a.getReal() == b.getReal() && a.getImag() > b.getImag()); };
			Sorter<ArraySequence<Complex>, Complex>::QuickSort(seq, pred);
			forn(i, 0, 9)
				Assert::IsTrue(seq[i].getReal() > seq[i + 1].getReal() || (seq[i].getReal() == seq[i + 1].getReal() && seq[i].getImag() <= seq[i + 1].getImag()));
		}

		TEST_METHOD(double10000qsort)
		{
			ifstream f;
			f.open("../Tests/TextDouble.txt");
			const int Number = 10000;
			ArraySequence<double> a;
			for (int i = 0; i < Number; i++)
			{
				double c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(double, double) = [](double a, double b) {return a > b; };
			Sorter<ArraySequence<double>, double>::QuickSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i] <= a[i + 1]);

			f.close();
		}

		TEST_METHOD(complex10000qsort_bRTbI)
		{
			ifstream f;
			f.open("../Tests/TextDouble.txt");
			const int Number = 10000;
			ArraySequence<Complex> a;
			for (int i = 0; i < Number; i++)
			{
				double c, d;
				f >> c >> d;
				a.Append(Complex(c, d));
			}
			bool (*pred)(Complex, Complex) = [](Complex a, Complex b) {return a.getReal() < b.getReal() || (a.getReal() == b.getReal() && a.getImag() > b.getImag()); };
			Sorter<ArraySequence<Complex>, Complex>::QuickSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsTrue(a[i].getReal() > a[i + 1].getReal() || (a[i].getReal() == a[i + 1].getReal() && a[i].getImag() <= a[i + 1].getImag()));

			f.close();
		}
	};

	TEST_CLASS(SortingStrings_Tests)
	{
		TEST_METHOD(lexicographicalOrderPredicate_tests)
		{
			string a = "abc", b = "abc";
			Assert::IsFalse(lexicographicalOrderPredicate(a, b));
			a = "a";
			Assert::IsTrue(lexicographicalOrderPredicate(b, a));
			Assert::IsFalse(lexicographicalOrderPredicate(a, b));
			b = "aa";
			Assert::IsTrue(lexicographicalOrderPredicate(b, a));
			Assert::IsFalse(lexicographicalOrderPredicate(a, b));
			a = "zaz";
			Assert::IsTrue(lexicographicalOrderPredicate(a, b));
			Assert::IsFalse(lexicographicalOrderPredicate(b, a));
			b = "zbz";
			Assert::IsTrue(lexicographicalOrderPredicate(b, a));
			Assert::IsFalse(lexicographicalOrderPredicate(a, b));
			a = "zazzzz";
			Assert::IsTrue(lexicographicalOrderPredicate(b, a));
			Assert::IsFalse(lexicographicalOrderPredicate(a, b));
			a = "";
			Assert::IsTrue(lexicographicalOrderPredicate(b, a));
			Assert::IsFalse(lexicographicalOrderPredicate(a, b));
		}

		TEST_METHOD(lexicographical1000RandomQSort)
		{
			ifstream f;
			f.open("../Tests/TextString.txt");
			int Number;
			f >> Number;
			ArraySequence<std::string> a;
			for (int i = 0; i < Number; i++)
			{
				string c;
				f >> c;
				a.Append(c);
			}
			bool (*pred)(string, string) = lexicographicalOrderPredicate;
			Sorter<ArraySequence<string>, string>::QuickSort(a, pred);
			forn(i, 0, Number - 1)
				Assert::IsFalse(lexicographicalOrderPredicate(a[i], a[i + 1]));

			f.close();
		}
	};
}

namespace TimeTests
{
	TEST_CLASS(TimeMarkerTests)
	{
		TEST_METHOD(QuickSortInt)
		{
			string filepath = "C:\\Users\\Максим\\Downloads\\LAB1_3sem\\Tests\\Text.txt";

			function<void(ArraySequence<int>&, bool (*)(int, int))> sort = Sorter<ArraySequence<int>, int>::QuickSort;

			long long res = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, filepath);
			Assert::IsTrue(res > 10 && res < 60);
		}

		TEST_METHOD(QuickSortDouble)
		{
			string filepath = "C:\\Users\\Максим\\Downloads\\LAB1_3sem\\Tests\\TextDouble.txt";

			function<void(ArraySequence<double>&, bool (*)(double, double))> sort = Sorter<ArraySequence<double>, double>::QuickSort;

			long long res = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, filepath);
			Assert::IsTrue(res > 10 && res < 60);
		}

		TEST_METHOD(QuickSortComplex)
		{
			string filepath = "C:\\Users\\Максим\\Downloads\\LAB1_3sem\\Tests\\TextComplex.txt";

			function<void(ArraySequence<Complex>&, bool (*)(Complex, Complex))> sort = Sorter<ArraySequence<Complex>, Complex>::QuickSort;

			long long res = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, filepath);
			Assert::IsTrue(res > 10 && res < 60);
		}

		TEST_METHOD(BinInsSortInt)
		{
			string filepath = "C:\\Users\\Максим\\Downloads\\LAB1_3sem\\Tests\\Text.txt";

			function<void(ArraySequence<int>&, bool (*)(int, int))> sort = Sorter<ArraySequence<int>, int>::BinaryInsertionsSort;

			long long res = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, filepath);
			Assert::IsTrue(res > 3000 && res < 5000);
		}
	};
}