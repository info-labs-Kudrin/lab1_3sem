#pragma once
#include <iostream>
#include <cmath>

using namespace std;

class Complex
{
private:
	double x, y;
public:
	Complex();
	Complex(double x, double y);
	Complex(Complex x, Complex y);
	Complex(double x);

	double Module();

	operator double();
	Complex operator !();
	Complex operator +(Complex added);
	Complex operator -();
	Complex operator -(Complex added);
	Complex operator *(Complex b);
	Complex operator /(double b);
	Complex operator /(Complex b);
	Complex operator =(int b);
	Complex operator =(double b);
	bool operator ==(Complex& b);
	bool operator !=(Complex& b);

	void prt();
	double getReal();
	double getImag();
};