#pragma once
#include <iostream>
#include "Complex.h"
#include "LinkedList.h"


template <typename T>
class DynamicArray
{
private:
	T* theArr;
	size_t size;

	const int ERROR_INDEX_OUT_OF_RANGE = 1;
	const int ERROR_INVALID_SIZE = 2;
public:
	DynamicArray();
	DynamicArray(size_t size);
	DynamicArray(size_t size, T standard);
	DynamicArray(T* arr, size_t size);
	DynamicArray(const DynamicArray<T>& dynamicArray);
	void operator=(const DynamicArray<T>& dynamicArray);
	~DynamicArray();

	size_t getSize();
	T get(int index);
	void set(int index, const T& value);
	void resize(int size);
	void InsertAt(T value, int index);
	void Erase(int index);
	void Append(T value);

	T& operator[](int index);
	bool operator==(DynamicArray<T>& a);
	bool operator!=(DynamicArray<T>& a);
};

template<typename T>
DynamicArray<T>::DynamicArray()
{
	this->size = 0;
	this->theArr = 0;
}

template<typename T>
DynamicArray<T>::DynamicArray(size_t size) : DynamicArray(size, T(0)) { }

template<typename T>
inline DynamicArray<T>::DynamicArray(size_t size, T standard)
{
	this->size = size;
	this->theArr = new T[size];
	for (size_t i = 0; i < size; i++)
		this->theArr[i] = T(standard);
}

template <typename T>
DynamicArray<T>::DynamicArray(T* arr, size_t size)
{
	this->size = size;
	if (!size)
	{
		this->theArr = 0;
		return;
	}
	this->theArr = new T[size];
	for (size_t i = 0; i < size; i++)
		this->theArr[i] = arr[i];
}

template<typename T>
DynamicArray<T>::DynamicArray(const DynamicArray<T>& dynamicArray)
{
	this->size = dynamicArray.size;
	if (this->size == 0)
	{
		this->theArr = 0;
		return;
	}
	this->theArr = new T[this->size];
	for (size_t i = 0; i < this->size; i++)
		this->theArr[i] = dynamicArray.theArr[i];
}

template<typename T>
inline void DynamicArray<T>::operator=(const DynamicArray<T>& dynamicArray)
{
	this->~DynamicArray();
	this->size = dynamicArray.size;
	if (this->size == 0)
	{
		this->theArr = 0;
		return;
	}
	this->theArr = new T[this->size];
	for (size_t i = 0; i < this->size; i++)
		this->theArr[i] = dynamicArray.theArr[i];
}

template<typename T>
DynamicArray<T>::~DynamicArray()
{
	if (this->theArr != 0)
		delete[] this->theArr;
}

template<typename T>
size_t DynamicArray<T>::getSize()
{
	return this->size;
}

template<typename T>
T DynamicArray<T>::get(int index)
{
	if (index >= (int)size || index < 0)
	{
		throw ERROR_INDEX_OUT_OF_RANGE;
	}
	return this->theArr[index];
}

template<typename T>
void DynamicArray<T>::set(int index, const T& value)
{
	if (index >= (int)size || index < 0)
	{
		throw ERROR_INDEX_OUT_OF_RANGE;
	}
	this->theArr[index] = value;
}

template<typename T>
void DynamicArray<T>::resize(int size)
{
	if (size < 0)
	{
		throw ERROR_INVALID_SIZE;
		return;
	}
	T* newArr = new T[size];
	size_t minSize = (((size_t)size < this->size) ? size : this->size);
	for (size_t i = 0; i < minSize; i++)
		newArr[i] = this->theArr[i];
	/*for (size_t i = minSize; i < (size_t)size; i++)
		newArr[i] = T(0);*/
	delete[] this->theArr;
	this->size = size;
	this->theArr = newArr;
}

template<typename T>
void DynamicArray<T>::InsertAt(T value, int index)
{
	if (index > (int)size || index < 0)
		throw ERROR_INDEX_OUT_OF_RANGE;
	this->resize(size + 1);
	for (int i = size - 1; i > index; i--)
		swap(this->theArr[i], this->theArr[i - 1]);
	this->theArr[index] = value;
}

template<typename T>
void DynamicArray<T>::Erase(int index)
{
	if (index < 0 || index >= (int)size)
		throw ERROR_INDEX_OUT_OF_RANGE;
	T* newArr = new T[size - 1];
	for (int i = 0; i < index; i++)
		newArr[i] = this->theArr[i];
	for (size_t i = index; i < size - 1; i++)
		newArr[i] = this->theArr[i + 1];
	delete[] this->theArr;
	this->theArr = newArr;
	size--;
}

template<typename T>
inline void DynamicArray<T>::Append(T value)
{
	this->InsertAt(value, size);
}

template<typename T>
T& DynamicArray<T>::operator[](int index)
{
	if (index >= (int)size || index < 0)
	{
		cout << index << " " << size;
		throw ERROR_INDEX_OUT_OF_RANGE;
	}

	return this->theArr[index];
}

template<typename T>
bool DynamicArray<T>::operator==(DynamicArray<T>& a)
{
	if (this->size != a.size)
		return 0;
	for (size_t i = 0; i < this->size; i++)
		if (this->theArr[i] != a.theArr[i])
			return 0;
	return 1;
}

template<typename T>
inline bool DynamicArray<T>::operator!=(DynamicArray<T>& a)
{
	return !(this->operator==(a));
}
