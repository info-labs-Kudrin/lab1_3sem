#pragma once
#include"Complex.h"

template<typename T>
struct item
{
	item<T> *prev;
	item<T> *next;
	T value;
};

template<typename T>
class LinkedList
{
private:
	item<T>* first;
	item<T>* last;
	const int ERROR_INDEX_OUT_OF_RANGE = 1;
	const int ERROR_INVALID_SIZE = 2;

public:
	LinkedList(size_t size);
	LinkedList(size_t size, T standard);
	LinkedList();
	LinkedList(T* items, size_t size);
	LinkedList(const LinkedList <T>& list);
	void operator=(const LinkedList<T>& list);
	~LinkedList();

	T get(size_t index);
	void set(int index, const T& value);
	size_t getSize();
	void Append(T value);
	void Prepend(T value);
	void InsertAt(T value, int index);
	void Erase(int index);

	T getFirst();
	T getLast();

	//Concatenates list to this
	void Concat(LinkedList<T>& list);

	bool operator==(const LinkedList<T>& list);
	bool operator!=(const LinkedList<T>& list);
	T& operator[](int index);
	LinkedList<T>* GetSubList(size_t startIndex, size_t endIndex);
};



template<typename T>
LinkedList<T>::LinkedList(size_t size) : LinkedList(size, T(0)) { }

template<typename T>
inline LinkedList<T>::LinkedList(size_t size, T standard) : LinkedList()
{
	for (size_t i = 0; i < size; i++)
		this->Append(T(standard));
}

template<typename T>
LinkedList<T>::LinkedList()
{
	this->first = 0;
	this->last = 0;
}

template<typename T>
LinkedList<T>::LinkedList(T* items, size_t size) : LinkedList()
{
	if (items == 0 || !size)
	{
		return;
	}
	for (size_t i = 0; i < size; i++)
	{
		this->Append(items[i]);
	}
}

template<typename T>
LinkedList<T>::LinkedList(const LinkedList<T>& list) : LinkedList()
{
	if (list.first == 0)
		return;
	item<T>* cur = list.first;
	this->Append(cur->value);
	while (cur->next)
	{
		cur = cur->next;
		this->Append(cur->value);
	}
}

template<typename T>
inline void LinkedList<T>::operator=(const LinkedList<T>& list)
{
	this->~LinkedList();
	this->first = 0;
	this->last = 0;
	if (list.first == 0)
		return;
	item<T>* cur = list.first;
	this->Append(cur->value);
	while (cur->next)
	{
		cur = cur->next;
		this->Append(cur->value);
	}
}

template<typename T>
LinkedList<T>::~LinkedList()
{
	item<T>* cur = this->first;
	while (cur != 0)
	{
		item<T>* prev = cur;
		cur = cur->next;
		delete prev;
	}
}

template<typename T>
T LinkedList<T>::get(size_t index)
{
	item<T>* cur = this->first;
	for (size_t i = 0; i < index; i++)
		if (!cur)
			throw ERROR_INDEX_OUT_OF_RANGE;
		else
			cur = cur->next;
	if (!cur)
		throw ERROR_INDEX_OUT_OF_RANGE;
	return cur->value;
}

template<typename T>
void LinkedList<T>::set(int index, const T& value)
{
	(*this)[index] = value;
}

template<typename T>
size_t LinkedList<T>::getSize()
{
	if (this->first == 0)
		return 0;
	size_t res = 1;
	item<T>* cur = this->first;
	while (cur->next != 0)
	{
		cur = cur->next;
		res++;
	}
	return res;
}

template<typename T>
void LinkedList<T>::Append(T value)
{
	if (this->last == 0)
	{
		item<T>* cur = new item<T>;
		this->last = cur;
		this->first = cur;
		cur->next = 0;
		cur->prev = 0;
		cur->value = value;
		return;
	}
	item<T>* cur = this->last;
	cur->next = new item<T>;
	item<T>* prev = cur;
	cur = cur->next;
	cur->prev = prev;
	cur->next = 0;
	cur->value = value;
	this->last = cur;
}

template<typename T>
void LinkedList<T>::Prepend(T value)
{
	if (this->first == 0)
	{
		item<T>* cur = new item<T>;
		this->last = cur;
		this->first = cur;
		cur->next = 0;
		cur->prev = 0;
		cur->value = value;
		return;
	}
	item<T>* cur = this->first;
	cur->prev = new item<T>;
	item<T>* next = cur;
	cur = cur->prev;
	cur->prev = 0;
	cur->next = next;
	cur->value = value;
	this->first = cur;
}

template<typename T>
void LinkedList<T>::InsertAt(T value, int index)
{
	int size = this->getSize();
	if (index < 0 || index > size)
		throw ERROR_INDEX_OUT_OF_RANGE;
	if (index == 0)
	{
		this->Prepend(value);
		return;
	}
	item<T>* cur = this->first;
	if (cur == 0 || index == size)
	{
		this->Append(value);
		return;
	}
	for (int i = 0; i < index; i++)
		if (cur == 0)
			throw ERROR_INDEX_OUT_OF_RANGE;
		else
			cur = cur->next;
	item<T>* prev = cur->prev, * nw;
	nw = new item<T>;
	nw->value = value;
	nw->prev = prev;
	prev->next = nw;
	nw->next = cur;
	cur->prev = nw;
}

template<typename T>
void LinkedList<T>::Erase(int index)
{
	if (index < 0)
		throw ERROR_INDEX_OUT_OF_RANGE;
	item<T>* cur = this->first, * prev, * next;
	for (int i = 0; i < index; i++)
	{
		if (cur == 0)
			throw ERROR_INDEX_OUT_OF_RANGE;
		cur = cur->next;
	}
	if (cur == 0)
		throw ERROR_INDEX_OUT_OF_RANGE;
	next = cur->next;
	prev = cur->prev;
	delete cur;
	if (next != 0)
		next->prev = prev;
	else
	{
		this->last = prev;
	}
	if (prev != 0)
		prev->next = next;
	else
	{
		this->first = next;
	}
}

template<typename T>
T LinkedList<T>::getFirst()
{
	if (this->first == 0)
		throw ERROR_INDEX_OUT_OF_RANGE;
	return this->first->value;
}

template<typename T>
T LinkedList<T>::getLast()
{
	if (this->last == 0)
		throw ERROR_INDEX_OUT_OF_RANGE;
	return this->last->value;
}

template<typename T>
void LinkedList<T>::Concat(LinkedList<T>& list)
{
	item<T>* cur = list.first;
	while (cur != 0)
	{
		this->Append(cur->value);
		cur = cur->next;
	}
}

template<typename T>
bool LinkedList<T>::operator==(const LinkedList<T>& list)
{
	item<T>* p1 = this->first, * p2 = list.first;
	if (p1 == 0 || p2 == 0)
		if (p1 == p2)
			return 1;
		else
			return 0;
	while (p1 != 0 && p2 != 0 && p1->value == p2->value)
	{
		p1 = p1->next;
		p2 = p2->next;
	}
	if (p1 == 0 || p2 == 0)
		if (p1 != p2)
			return 0;
		else
			return 1;
	return 0;
}

template<typename T>
inline bool LinkedList<T>::operator!=(const LinkedList<T>& list)
{
	return !(this->operator==(list));
}

template<typename T>
T& LinkedList<T>::operator[](int index)
{
	if (index < 0)
		throw ERROR_INDEX_OUT_OF_RANGE;
	item<T>* cur = this->first;
	for (int i = 0; i < index; i++)
		if (cur == 0)
			throw ERROR_INDEX_OUT_OF_RANGE;
		else
			cur = cur->next;
	if (cur == 0)
		throw ERROR_INDEX_OUT_OF_RANGE;
	return cur->value;
}
template<typename T>
LinkedList<T>* LinkedList<T>::GetSubList(size_t startIndex, size_t endIndex)
{
	if (endIndex < startIndex)
		swap(startIndex, endIndex);
	item<T>* t1 = this->first, * t2;
	for (size_t i = 0; i < startIndex; i++)
	{
		if (t1 == 0)
			throw ERROR_INDEX_OUT_OF_RANGE;
		t1 = t1->next;
	}
	t2 = t1;
	for (size_t i = startIndex; i < endIndex; i++)
	{
		if (t2 == 0)
			throw ERROR_INDEX_OUT_OF_RANGE;
		t2 = t2->next;
	}
	LinkedList<T>* res = new LinkedList<T>;
	while (t1 != t2)
	{
		res->Append(t1->value);
		t1 = t1->next;
	}
	res->Append(t1->value);
	return res;
}