#include "console.h"

Console::Console()
{
	arr = new ArraySequence<int>;
}

void Console::mainMenu() {
	cout << "########################\n";
	cout << "What do you want me to do?\n";
	cout << "0 -- Quit\n";
	cout << "1 -- Sort the ArraySequence on different integer data and get a graph\n";
	cout << "2 -- Fill the ArraySequence with new integer data via console\n";
	cout << "3 -- Fill the ArraySequence with new integer data via file\n";
	cout << "4 -- Sort the ArraySequence and count the time\n";
	cout << "5 -- Print the ArraySequence\n";
	cout << "6 -- Sort the ArraySequence on different double data and get a graph\n";
	cout << "7 -- Sort the ArraySequence on different complex data and get a graph\n";
	cout << "8 -- Sort the ArraySequence on different string data and get a graph\n";
	cout << "#######################\n";
	int choice;
	cin >> choice;
	switch (choice)
	{
	case 0: {
		exit(0);
	}
	case 1:
	{
		sortArrayGetGraphs();
		break;
	}
	case 2:
	{
		newArrSeq_console();
		break;
	}
	case 3:
	{
		newArrSeq_file();
		break;
	}
	case 4:
	{
		SortArrayGetTime();
		break;
	}
	case 5:
	{
		prtArr();
		break;
	}
	case 6:
	{
		sortArrayGetGraphs_double();
		break;
	}
	case 7:
	{
		sortArrayGetGraphs_complex();
		break;
	}
	case 8:
	{
		sortArrayGetGraphs_string();
		break;
	}
	default:
		Console::tryAgain();
		break;
	}
}

void Console::console()
{
	while (true)
		mainMenu();
}


void Console::tryAgain() {
	cout << "........................\n";
	cout << "Why don't you try again?\n";
	cout << "........................\n";
}

template<class T>
bool Console::getSort(function<void(ArraySequence<T>&, bool(*)(T, T))>& sort, string& name)
{
	cout << "------------------------\n";
	cout << "How do you want me to sort?\n";
	cout << "1 -- BubbleSort\n";
	cout << "2 -- ShakerSort\n";
	cout << "3 -- SimpleInsertionsSort\n";
	cout << "4 -- SimpleSelectionSort\n";
	cout << "5 -- BinaryInsertionsSort\n";
	cout << "6 -- QuickSort\n";
	cout << "------------------------\n";
	int choice;

	cin >> choice;
	switch (choice)
	{
	case 1:
	{
		sort = Sorter<ArraySequence<T>, T>::BubbleSort;
		name = "BubbleSort";
		break;
	}
	case 2:
	{
		sort = Sorter<ArraySequence<T>, T>::ShakerSort;
		name = "ShakerSort";
		break;
	}
	case 3:
	{
		sort = Sorter<ArraySequence<T>, T>::SimpleInsertionsSort;
		name = "SimpleInsertionsSort";
		break;
	}
	case 4:
	{
		sort = Sorter<ArraySequence<T>, T>::SimpleSelectionSort;
		name = "SimpleSelectionSort";
		break;
	}
	case 5:
	{
		sort = Sorter<ArraySequence<T>, T>::BinaryInsertionsSort;
		name = "BinaryInsertionsSort";
		break;
	}
	case 6:
	{
		sort = Sorter<ArraySequence<T>, T>::QuickSort;
		name = "QuickSort";
		break;
	}
	default:
		Console::tryAgain();
		return 0;
	}
}

void Console::fillTheHTML(ofstream& html, vector<long long> almostSorted, vector<long long> sorted, vector<long long> random, vector<long long> sortedBackwards, string name)
{
	html << "<!DOCTYPE html>" << endl;
	html << "<html>" << endl;
	html << "<head>" << endl;
	html << "<style>\n\tbody{\n\t\tbackground-color: #AA3333;\n\t}\n</style>\n";
	html << "<meta charset=\"utf-8\">" << endl;
	html << "<title>Lab3</title>" << endl;
	html << "<script src=\"https://www.google.com/jsapi\"></script>" << endl;
	html << "<script>" << endl;
	html << "google.load(\"visualization\", \"1\", {packages:[\"corechart\"]});" << endl;
	html << "google.setOnLoadCallback(drawChart);" << endl;
	html << "function drawChart() {" << endl;
	html << "var data = google.visualization.arrayToDataTable([" << endl;
	html << "[\'Amount of numbers\', \'sorted\', \'almost sorted\', \'random\', \'sorted backwards\']," << endl;
	html << "[\'500\', " << sorted[0] << ", " << almostSorted[0] << ", " << random[0] << ", " << sortedBackwards[0] << "]," << endl;
	html << "[\'1000\', " << sorted[1] << ", " << almostSorted[1] << ", " << random[1] << ", " << sortedBackwards[1] << "]," << endl;
	html << "[\'2000\', " << sorted[2] << ", " << almostSorted[2] << ", " << random[2] << ", " << sortedBackwards[2] << "]" << endl;
	html << "]);" << endl;
	html << "var options = {" << endl;
	html << "title: \'Sorting time (" << name << ")'," << endl;
	html << "hAxis: {title: 'Amount of data'}," << endl;
	html << "vAxis: {title: 'Milliseconds'}" << endl;
	html << "};" << endl;
	html << "var chart = new google.visualization.ColumnChart(document.getElementById(\'oil\'));" << endl;
	html << "chart.draw(data, options);" << endl;
	html << "}" << endl;
	html << "</script>" << endl;
	html << "</head>" << endl;
	html << "<body>" << endl;
	html << "<div id=\"oil\" style=\"width: 1500px; height: 400px;\"></div>" << endl;
	html << "</body>" << endl;
	html << "</html>" << endl;
}

void Console::sortArrayGetGraphs()
{
	function<void(ArraySequence<int>&, bool (*)(int, int))> sort;
	string name;
	if (!getSort<int>(sort, name))
		return;
	vector<long long> almostSorted(3), sorted(3), random(3), sortedBackwards(3);
	
	cout << "Calculating...\n";

	almostSorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted500.txt");
	almostSorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted1000.txt");
	almostSorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted2000.txt");

	cout << "3...\n";

	sorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\Sorted500.txt");
	sorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\Sorted1000.txt");
	sorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\Sorted2000.txt");

	cout << "2...\n";

	random[0] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\Random500.txt");
	random[1] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\Random1000.txt");
	random[2] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\Random2000.txt");

	cout << "1...\n";

	sortedBackwards[0] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards500.txt");
	sortedBackwards[1] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards1000.txt");
	sortedBackwards[2] = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards2000.txt");

	ofstream html;
	html.open("site.html");
	name += " integer data";
	fillTheHTML(html, almostSorted, sorted, random, sortedBackwards, name);

	html.close();

	system("cd C:\\Program Files (x86)\\Google\\Chrome\\Application && chrome.exe C:\\Users\\������\\Downloads\\LAB1_3sem\\LAB1_3sem\\site.html");
}

void Console::sortArrayGetGraphs_double()
{
	function<void(ArraySequence<double>&, bool (*)(double, double))> sort;
	string name;
	if (!getSort<double>(sort, name))
		return;
	vector<long long> almostSorted(3), sorted(3), random(3), sortedBackwards(3);

	cout << "Calculating...\n";

	almostSorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted500double.txt");
	almostSorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted1000double.txt");
	almostSorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted2000double.txt");

	cout << "3...\n";

	sorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\Sorted500double.txt");
	sorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\Sorted1000double.txt");
	sorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\Sorted2000double.txt");

	cout << "2...\n";

	random[0] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\Random500double.txt");
	random[1] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\Random1000double.txt");
	random[2] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\Random2000double.txt");

	cout << "1...\n";

	sortedBackwards[0] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards500double.txt");
	sortedBackwards[1] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards1000double.txt");
	sortedBackwards[2] = IntTimeMarker::MillisecondTime<ArraySequence<double>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards2000double.txt");

	ofstream html;
	html.open("site.html");

	name += " double data";
	fillTheHTML(html, almostSorted, sorted, random, sortedBackwards, name);

	html.close();

	system("cd C:\\Program Files (x86)\\Google\\Chrome\\Application && chrome.exe C:\\Users\\������\\Downloads\\LAB1_3sem\\LAB1_3sem\\site.html");
}

void Console::sortArrayGetGraphs_complex()
{
	function<void(ArraySequence<Complex>&, bool (*)(Complex, Complex))> sort;
	string name;
	if (!getSort<Complex>(sort, name))
		return;
	vector<long long> almostSorted(3), sorted(3), random(3), sortedBackwards(3);

	cout << "Calculating...\n";

	almostSorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted500complex.txt");
	almostSorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted1000complex.txt");
	almostSorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted2000complex.txt");

	cout << "3...\n";

	sorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\Sorted500complex.txt");
	sorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\Sorted1000complex.txt");
	sorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\Sorted2000complex.txt");

	cout << "2...\n";

	random[0] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\Random500complex.txt");
	random[1] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\Random1000complex.txt");
	random[2] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\Random2000complex.txt");

	cout << "1...\n";

	sortedBackwards[0] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards500complex.txt");
	sortedBackwards[1] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards1000complex.txt");
	sortedBackwards[2] = IntTimeMarker::MillisecondTime<ArraySequence<Complex>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards2000complex.txt");

	ofstream html;
	html.open("site.html");

	name += " complex data";
	fillTheHTML(html, almostSorted, sorted, random, sortedBackwards, name);

	html.close();

	system("cd C:\\Program Files (x86)\\Google\\Chrome\\Application && chrome.exe C:\\Users\\������\\Downloads\\LAB1_3sem\\LAB1_3sem\\site.html");
}

void Console::sortArrayGetGraphs_string()
{
	function<void(ArraySequence<string>&, bool (*)(string, string))> sort;
	string name;
	if (!getSort<string>(sort, name))
		return;
	vector<long long> almostSorted(3), sorted(3), random(3), sortedBackwards(3);

	cout << "Calculating...\n";

	almostSorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted500string.txt");
	almostSorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted1000string.txt");
	almostSorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\AlmostSorted2000string.txt");

	cout << "3...\n";

	sorted[0] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\Sorted500string.txt");
	sorted[1] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\Sorted1000string.txt");
	sorted[2] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\Sorted2000string.txt");

	cout << "2...\n";

	random[0] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\Random500string.txt");
	random[1] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\Random1000string.txt");
	random[2] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\Random2000string.txt");

	cout << "1...\n";

	sortedBackwards[0] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards500string.txt");
	sortedBackwards[1] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards1000string.txt");
	sortedBackwards[2] = IntTimeMarker::MillisecondTime<ArraySequence<string>>(sort, "..\\LAB1_3sem\\data\\SortedBackwards2000string.txt");

	ofstream html;
	html.open("site.html");

	name += " string data";
	fillTheHTML(html, almostSorted, sorted, random, sortedBackwards, name);

	html.close();

	system("cd C:\\Program Files (x86)\\Google\\Chrome\\Application && chrome.exe C:\\Users\\������\\Downloads\\LAB1_3sem\\LAB1_3sem\\site.html");
}

void Console::newArrSeq_console()
{
	int n;
	cout << "------------------------\n";
	cout << "Type number of the elements...\n";
	cin >> n;
	arr->resize(n);
	cout << "Type " << n << " elements...\n";
	for (int i = 0; i < n; i++)
	{
		int c;
		cin >> c;
		(*arr)[i] = c;
	}
}

void Console::newArrSeq_file()
{
	string path;
	cout << "------------------------\n";
	cout << "Type full path to the file (the first number in the file means the amount of numbers I will read)...\n";
	getline(cin, path);
	getline(cin, path);
	ifstream f;
	f.open(path);
	int n;
	f >> n;
	arr->resize(n);
	for (int i = 0; i < n; i++)
	{
		int c;
		f >> c;
		(*arr)[i] = c;
	}
	cout << "Done\n";
	f.close();
}

void Console::SortArrayGetTime()
{
	function<void(ArraySequence<int>&, bool (*)(int, int))> sort;
	cout << "------------------------\n";
	cout << "How do you want me to sort?\n";
	cout << "1 -- BubbleSort\n";
	cout << "2 -- ShakerSort\n";
	cout << "3 -- SimpleInsertionsSort\n";
	cout << "4 -- SimpleSelectionSort\n";
	cout << "5 -- BinaryInsertionsSort\n";
	cout << "6 -- QuickSort\n";
	cout << "------------------------\n";
	int choice;
	string name;
	cin >> choice;
	switch (choice)
	{
	case 1:
	{
		sort = Sorter<ArraySequence<int>, int>::BubbleSort;
		break;
	}
	case 2:
	{
		sort = Sorter<ArraySequence<int>, int>::ShakerSort;
		break;
	}
	case 3:
	{
		sort = Sorter<ArraySequence<int>, int>::SimpleInsertionsSort;
		break;
	}
	case 4:
	{
		sort = Sorter<ArraySequence<int>, int>::SimpleSelectionSort;
		break;
	}
	case 5:
	{
		sort = Sorter<ArraySequence<int>, int>::BinaryInsertionsSort;
		break;
	}
	case 6:
	{
		sort = Sorter<ArraySequence<int>, int>::QuickSort;
		break;
	}
	default:
		Console::tryAgain();
		return;
	}
	ofstream f;
	f.open("working.txt");
	f << arr->getSize() << endl;
	for (int i = 0; i < arr->getSize(); i++)
		f << (*arr)[i] << " ";
	f.close();
	cout << "Calculating...\n";
	long long res = IntTimeMarker::MillisecondTime<ArraySequence<int>>(sort, "../LAB1_3sem/working.txt");
	cout << "The prosess took " << res << " milliseconds\n";
}

void Console::prtArr()
{
	int sz = arr->getSize();
	cout << "There ara " << sz << " elements in the array:\n";
	for (int i = 0; i < sz; i++)
		cout << (*arr)[i] << " ";
	cout << endl;
}
