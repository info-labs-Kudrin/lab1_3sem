#pragma once
#include "Sorter.h"
#include "Sequence.h"
#include "Complex.h"

#include <functional>
#include <string>
#include <chrono>
#include <fstream>

class IntTimeMarker
{
	//Give me sort function and full path to the file with the data (first comes n -- amount of numbers -- the come n numbers), I will return time in milliseconds
private:
	static double getDouble(char* buf, size_t sz, int& wordsize);
public:
	template <class ArrOrSeq>
	static long long MillisecondTime(function<void(ArrOrSeq&, bool (*)(int, int))> sort, string filepath);
	template <class ArrOrSeq>
	static long long MillisecondTime(function<void(ArrOrSeq&, bool (*)(double, double))> sort, string filepath);
	template <class ArrOrSeq>
	static long long MillisecondTime(function<void(ArrOrSeq&, bool (*)(Complex, Complex))> sort, string filepath);
	template <class ArrOrSeq>
	static long long MillisecondTime(function<void(ArrOrSeq&, bool (*)(string, string))> sort, string filepath);
};

template<class ArrOrSeq>
inline long long IntTimeMarker::MillisecondTime(function<void(ArrOrSeq&, bool(*)(int, int))> sort, string filepath)
{
	const int N = 80000;

	const char* filename = filepath.c_str(); 
	char * buffer = new char[N];
	int* Arr = new int[N / 8];
	FILE* f;
	fopen_s(&f, filename, "r");
	fread(buffer, 1, N, f);

	int n = 0, j = 0, k = 0;
	while (buffer[j] >= '0' && buffer[j] <= '9')
	{
		n = n * 10 + buffer[j] - '0';
		j++;
	}
	j++;
	while (j < N && k < n)
	{
		int c = 0;
		while (buffer[j] >= '0' && buffer[j] <= '9')
		{
			c = c * 10 + buffer[j] - '0';
			j++;
		}
		j++;
		Arr[k] = c;
		k++;
	}
	ArrOrSeq arr(Arr, k);


	//Засекаем
	auto start = chrono::high_resolution_clock::now();
	//
	sort(arr, [](int a, int b) {return a > b; });
	//Отсекаем
	auto finish = chrono::high_resolution_clock::now();
	//
	fclose(f);
	chrono::duration<float> dur = (finish - start);
	int res = int(1000.0 * dur.count());

	delete[] Arr;
	delete[] buffer;
	return res;
}

template<class ArrOrSeq>
inline long long IntTimeMarker::MillisecondTime(function<void(ArrOrSeq&, bool(*)(double, double))> sort, string filepath)
{
	const int N = 10000*sizeof(double);

	const char* filename = filepath.c_str();
	char* buffer = new char[N];
	double* Arr = new double[N / sizeof(double)];
	FILE* f;
	fopen_s(&f, filename, "r");
	fread(buffer, 1, N, f);

	int n = 0, j = 0, k = 0;
	while (buffer[j] >= '0' && buffer[j] <= '9')
	{
		n = n * 10 + buffer[j] - '0';
		j++;
	}
	j++;
	while (j < N && k < n)
	{
		int wdSz = 0;
		double c = getDouble(buffer + j, N - j, wdSz);
		j += wdSz + 1;
		Arr[k] = c;
		k++;
	}
	ArrOrSeq arr(Arr, k);


	//Засекаем
	auto start = chrono::high_resolution_clock::now();
	//
	sort(arr, [](double a, double b) {return a > b; });
	//Отсекаем
	auto finish = chrono::high_resolution_clock::now();
	//
	fclose(f);
	chrono::duration<float> dur = (finish - start);
	int res = int(1000.0 * dur.count());

	delete[] Arr;
	delete[] buffer;
	return res;
}

template<class ArrOrSeq>
inline long long IntTimeMarker::MillisecondTime(function<void(ArrOrSeq&, bool(*)(Complex, Complex))> sort, string filepath)
{
	const int N = 15000 * sizeof(Complex);

	const char* filename = filepath.c_str();
	char* buffer = new char[N];
	Complex* Arr = new Complex[N / sizeof(Complex)];
	FILE* f;
	fopen_s(&f, filename, "r");
	fread(buffer, 1, N, f);

	int n = 0, j = 0, k = 0;
	while (buffer[j] >= '0' && buffer[j] <= '9')
	{
		n = n * 10 + buffer[j] - '0';
		j++;
	}
	j++;
	while (j < N && k < n)
	{
		int wdSz = 0;
		double Re = getDouble(buffer + j, N - j, wdSz);
		j += wdSz + 1; wdSz = 0;
		double Im = getDouble(buffer + j, N - j, wdSz);
		Complex c(Re, Im);
		j += wdSz + 1;
		Arr[k] = c;
		k++;
	}
	ArrOrSeq arr(Arr, k);


	//Засекаем
	auto start = chrono::high_resolution_clock::now();
	//
	sort(arr, [](Complex a, Complex b) {return a.getReal() > b.getReal() || (a.getReal() == b.getReal() && a.getImag() > b.getImag()); });
	//Отсекаем
	auto finish = chrono::high_resolution_clock::now();
	//
	fclose(f);
	chrono::duration<float> dur = (finish - start);
	int res = int(1000.0 * dur.count());

	delete[] Arr;
	delete[] buffer;
	return res;
}


template<class ArrOrSeq>
inline long long IntTimeMarker::MillisecondTime(function<void(ArrOrSeq&, bool(*)(string, string))> sort, string filepath)
{
	const int N = 10000 * 9;

	const char* filename = filepath.c_str();
	char* buffer = new char[N];
	string* Arr = new string[N / 9];
	FILE* f;
	fopen_s(&f, filename, "r");
	fread(buffer, 1, N, f);

	int n = 0, j = 0, k = 0;
	while (buffer[j] >= '0' && buffer[j] <= '9')
	{
		n = n * 10 + buffer[j] - '0';
		j++;
	}
	j++;
	while (j < N && k < n)
	{
		string s = "";
		while (buffer[j] != ' ' && buffer[j] != '\n' && buffer[j] != '\t')
		{
			s = s + buffer[j];
			j++;
		}
		j++;
		Arr[k] = s;
		k++;
	}
	ArrOrSeq arr(Arr, k);


	//Засекаем
	auto start = chrono::high_resolution_clock::now();
	//
	sort(arr, lexicographicalOrderPredicate);
	//Отсекаем
	auto finish = chrono::high_resolution_clock::now();
	//
	fclose(f);
	chrono::duration<float> dur = (finish - start);
	int res = int(1000.0 * dur.count());

	delete[] Arr;
	delete[] buffer;
	return res;
}
