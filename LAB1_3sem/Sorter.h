#pragma once
#include <string>

/// <summary>
/// Predicate must return true if first value is to be put later
/// Example of referring to the class:
/// Sorter<ArraySequence<int>, int>::BubbleSort(seq, pred);
/// </summary>
/// <typeparam name="ArrOrList">ArraySequence or LListSequence</typeparam>
/// <typeparam name="T"></typeparam>
template <class ArrOrList, class T>
class Sorter {

private:
	static void QuickSort_recurrent(ArrOrList& seq, bool (*predicate)(T, T), int l, int r);

public:
	static void BubbleSort(ArrOrList &seq, bool (*predicate)(T, T));
	static void ShakerSort(ArrOrList &seq, bool (*predicate)(T, T));
	static void SimpleInsertionsSort(ArrOrList &seq, bool (*predicate)(T, T));
	static void SimpleSelectionSort(ArrOrList &seq, bool (*predicate)(T, T));
	static void BinaryInsertionsSort(ArrOrList &seq, bool (*predicate)(T, T));
	static void QuickSort(ArrOrList &seq, bool (*predicate)(T, T));
};

bool lexicographicalOrderPredicate(std::string a, std::string b);

template<class ArrOrList, class T>
inline void Sorter<ArrOrList, T>::BubbleSort(ArrOrList &seq, bool(*predicate)(T, T))
{
	size_t sz = seq.getSize();
	for (size_t i = 0; i < sz; i++)
		for (unsigned int j = 0; j < sz - i - 1; j++)
			if (predicate(seq[j], seq[j + 1]))
				swap(seq[j], seq[j + 1]);
}

template<class ArrOrList, class T>
inline void Sorter<ArrOrList, T>::ShakerSort(ArrOrList &seq, bool(*predicate)(T, T))
{
	bool swapped = 0;
	int sz = seq.getSize(), iteration = 0;
	do
	{
		swapped = 0;
		for (int i = iteration; i < sz - 1 - iteration; i++)
			if (predicate(seq[i], seq[i + 1]))
				swap(seq[i], seq[i + 1]);
		for (int i = sz - 1 - iteration; i > iteration; i--)
			if (predicate(seq[i - 1], seq[i]))
			{
				swap(seq[i - 1], seq[i]);
				swapped = 1;
			}

	} while (swapped);
}

template<class ArrOrList, class T>
inline void Sorter<ArrOrList, T>::SimpleInsertionsSort(ArrOrList &seq, bool(*predicate)(T, T))
{
	int sz = seq.getSize();
	for (int i = 0; i < sz; i++)
	{
		int j = i - 1;
		while (j >= 0 && predicate(seq[j], seq[j + 1]))
		{
			swap(seq[j], seq[j + 1]);
			j--;
		}
	}
}

template<class ArrOrList, class T>
inline void Sorter<ArrOrList, T>::SimpleSelectionSort(ArrOrList &seq, bool(*predicate)(T, T))
{
	int sz = seq.getSize();
	for (int iter = 0; iter < sz; iter++)
	{
		int maIndex = 0;
		for (int i = 1; i < sz - iter; i++)
			if (predicate(seq[i], seq[maIndex]))
				maIndex = i;
		swap(seq[maIndex], seq[sz - 1 - iter]);
	}
}

template<class ArrOrList, class T>
inline void Sorter<ArrOrList, T>::BinaryInsertionsSort(ArrOrList &seq, bool(*predicate)(T, T))
{
	int sz = seq.getSize();
	for (int i = 1; i < sz; i++)
	{
		int l = 0, r = i - 1;
		while (r - l > 1) {
			int mid = (l + r) / 2;
			if (predicate(seq[mid], seq[i]))
				r = mid;
			else
				l = mid;
		}
		int start;
		if (predicate(seq[i], seq[r]))
			start = r + 1;
		else if (predicate(seq[i], seq[l]))
			start = r;
		else
			start = l;
		for (int j = i; j > start; j--)
			swap(seq[j - 1], seq[j]);
	}
}


template<class ArrOrList, class T>
inline void Sorter<ArrOrList, T>::QuickSort_recurrent(ArrOrList& seq, bool(*predicate)(T, T), int l, int r)
{
	if (r <= l)
		return;
	int r0 = r, l0 = l;

	if (r0 - l0 == 1)
	{
		if (predicate(seq[l0], seq[r0]))
			swap(seq[l0], seq[r0]);
		return;
	}

	T p = seq[(l + r) / 2];

	do {
		while (l < r && predicate(p, seq[l])) l++;
		while (l < r && predicate(seq[r], p)) r--;

		if (l < r) {
			swap(seq[l], seq[r]);
			l++; r--;
		}
	} while (l < r);

	QuickSort_recurrent(seq, predicate, l0, r);
	QuickSort_recurrent(seq, predicate, l, r0);
}

template<class ArrOrList, class T>
inline void Sorter<ArrOrList, T>::QuickSort(ArrOrList &seq, bool(*predicate)(T, T))
{
	QuickSort_recurrent(seq, predicate, 0, seq.getSize() - 1);
}