#include "Sorter.h"

using namespace std;

bool lexicographicalOrderPredicate(string a, string b)
{
	int i = 0, asz = a.size(), bsz = b.size();
	while (i < asz && i < bsz && a[i] == b[i])
		i++;
	if (i == asz || i == bsz)
	{
		if (asz > bsz)
			return 1;
		return 0;
	}
	if (a[i] > b[i])
		return 1;
	return 0;
}