#include "IntTimeMarker.h"

double IntTimeMarker::getDouble(char* buf, size_t sz, int& wordsize)
{
	int whole = 0, fract = 0, j = 0;
	bool neg = (buf[0] == '-');
	if (buf[0] == '-')
		j++;
	
	while (j < sz && buf[j] >= '0' && buf[j] <= '9')
	{
		whole = whole * 10 + buf[j] - '0';
		j++;
	}

	if (buf[j] != '.')
	{
		wordsize = j;
		return whole;
	}
	j++;

	while (j < sz && buf[j] >= '0' && buf[j] <= '9')
	{
		fract = fract * 10 + buf[j] - '0';
		j++;
	}

	wordsize = j;
	int k = 0, fractCopy = fract;
	do {
		fractCopy /= 10;
		k++;
	} while (fractCopy != 0);
	double res = whole + fract / pow(10, k);
	if (neg)
		res *= -1;
	return res;
}