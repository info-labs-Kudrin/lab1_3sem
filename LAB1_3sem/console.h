#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <functional>

#include "Sequence.h"
#include "Sorter.h"
#include "IntTimeMarker.h"

using namespace std;

class Console {
private:

	ArraySequence<int> *arr;

	template<class T>
	bool getSort(function<void(ArraySequence<T>&, bool (*)(T, T))>& sort, string& name);
	void fillTheHTML(ofstream& html, vector<long long> almostSorted, vector<long long> sorted, vector<long long> random, vector<long long> sortedBackwards, string name);

	void sortArrayGetGraphs();
	void sortArrayGetGraphs_double();
	void sortArrayGetGraphs_complex();
	void sortArrayGetGraphs_string();

	void SortArrayGetTime();
	void newArrSeq_file();
	void newArrSeq_console();
	void prtArr();

	void tryAgain();
	void mainMenu();
public:
	Console();
	void console();
};